#!/bin/sh

if [ "$(id -u)" -eq 0 ]; then
  echo "This script must be run as root" 
  exit 1
fi

clear
lsblk
printf "Select instalation drive (drive name): "
read -r sel_drive
cfdisk /dev/"$sel_drive"

clear
printf "1.OpenRC
2.runit
3.s6
4.suit66
5.dinit
Select init (1,2,3,4,5): "
read -r sel_init

clear
printf "Enable encryption (y,n): "
read -r sel_enc

clear
lsblk
printf "Select boot partition (partition name): "
read -r sel_boot

clear
lsblk
printf "Select root partition (partition name): "
read -r sel_root

clear
lsblk
printf "Use seperate home partition (partition name,n): "
read -r sel_home

if [ "$sel_home" != "n" ]; then
  clear
  printf "Format home partition (y,n): "
  read -r sel_home_format

  if [ "$sel_enc" = "y" ] && [ "$sel_home_format" = "n" ]; then
    printf "Enter location for keyfile: "
    read -r sel_home_keyfile
  fi
fi

#setup root
if [ "$sel_enc" = "y" ]; then
  cryptsetup -y -v luksFormat /dev/"$sel_root"
  cryptsetup open /dev/"$sel_root" cryptroot
  mkfs.ext4 -F -L root /dev/mapper/cryptroot
  mount /dev/mapper/cryptroot /mnt
else
  mkfs.ext4 -F -L root /dev/"$sel_root"
  mount /dev/"$sel_root" /mnt
fi

#setup /boot
mkfs.vfat -F 32 /dev/"$sel_boot"
fatlabel /dev/"$sel_boot" BOOT
mkdir /mnt/boot/
mount /dev/"$sel_boot" /mnt/boot

#setup /home
mkdir /mnt/home
if [ "$sel_enc" = "y" ]; then
  mkdir /mnt/etc
  
  if [ "$sel_home_format" = "n" ]; then
    cp "$sel_home_keyfile" /mnt/etc/crypthome.key
    chmod 400 /mnt/etc/crypthome.key
    cryptsetup luksOpen /dev/"$sel_home" crypthome --key-file /mnt/etc/crypthome.key
  else
    openssl genrsa -out /mnt/etc/crypthome.key
    chmod 400 /mnt/etc/crypthome.key
    cryptsetup -y -v luksFormat /dev/"$sel_home" --key-file /mnt/etc/crypthome.key
    cryptsetup luksOpen /dev/"$sel_home" crypthome --key-file /mnt/etc/crypthome.key
    mkfs.ext4 -F -L home /dev/mapper/crypthome
  fi

  mount /dev/mapper/crypthome /mnt/home
else
  [ "$sel_home_format" = "y" ] && mkfs.ext4 -F -L root /dev/"$sel_home"
  mount /dev/"$sel_home" /mnt/home
fi

#mount tables
fstabgen -U /mnt >> /mnt/etc/fstab
sed -i "s/ordered/ordered,discard/g" /mnt/etc/fstab
echo "tmpfs	/tmp	tmpfs	nodev,nosuid,size=1G	0 0" >> /mnt/etc/fstab
[ "$sel_enc" = "y" ] && echo "homecrypt /dev/$sel_home       /etc/crypthome.key" >> /mnt/etc/crypttab

#basestrap
sed -i '/ParallelDownloads/s/^#//g' /etc/pacman.conf
pacman --noconfirm -Sy artix-keyring
case "$sel_init" in
	1) basestrap /mnt base base-devel openrc elogind-openrc linux linux-firmware || basestrap /mnt base base-devel openrc elogind-openrc linux linux-firmware
          ;;
	2) basestrap /mnt base base-devel runit elogind-runit linux linux-firmware || basestrap /mnt base base-devel runit elogind-runit linux linux-firmware
          ;;
	3) basestrap /mnt base base-devel s6-base elogind-s6 linux linux-firmware || basestrap /mnt base base-devel s6-base elogind-s6 linux linux-firmware
          ;;
	4) basestrap /mnt base base-devel 66 elogind-suite66 linux linux-firmware || basestrap /mnt base base-devel 66 elogind-suite66 linux linux-firmware
          ;;
        5) basestrap /mnt base base-devel dinit elogind-dinit linux linux-firmware || basestrap /mnt base base-devel dinit elogind-dinit linux linux-firmware
          ;;
esac

# prepare part2
sed '1,/^# part2$/d' "$0" > /mnt/artix_part2.sh &&
sed -i "1s/^/sel_init=$sel_init\n/" /mnt/artix_part2.sh
sed -i "1s/^/sel_boot=$sel_boot\n/" /mnt/artix_part2.sh
chmod +x /mnt/artix_part2.sh
artix-chroot /mnt ./artix_part2.sh
exit

# part2
sed -i '/ParallelDownloads/s/^#//g' /etc/pacman.conf
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
hwclock --systohc
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
printf "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf

# install packages
pacman --noconfirm -S efibootmgr linux-headers pipewire pipewire-pulse alsa-utils neovim wget git connman || pacman --noconfirm -S efibootmgr linux-headers pipewire pipewire-pulse alsa-utils neovim wget git connman

# enable services
case "$sel_init" in
	1) pacman --noconfirm -S connman-openrc iwd-openrc cryptsetup-openrc &&
	 rc-update add connmand
	 rc-update add iwd
          ;;
	2) pacman --noconfirm -S connman-runit iwd-runit cryptsetup-runit &&
	 ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default
	 ln -s /etc/runit/sv/iwd /etc/runit/runsvdir/default
          ;;
	3) pacman --noconfirm -S connman-s6 iwd-s6 cryptsetup-s6 &&
	 s6-rc-bundle-update -c /etc/s6/rc/compiled add default connmand
	 s6-rc-bundle-update -c /etc/s6/rc/compiled add default iwd

          ;;
	4) pacman --noconfirm -S connman-suite66 iwd-suite66 cryptsetup-suite66 &&
	 66-tree -ncE default
	 #Assuming your default tree is named "default"
	 66-enable -t default connmand
	 66-enable -t default iwd
          ;;
esac


# pc specific configuration
det_cpu=$(grep -m1 'vendor' /proc/cpuinfo | cut -d" " -f 2)
case "$det_cpu" in
	AuthenticAMD) pacman --noconfirm -S intel-ucode || pacman --noconfirm -S intel-ucode
          ;;
	GenuineIntel) pacman --noconfirm -S amd-ucode || pacman --noconfirm -S amd-ucode
          ;;
esac

#final steps
clear
printf "Enter Hostname: "
read -r hostname

printf "%s" "$hostname" > /etc/hostname
printf "127.0.0.1       localhost
::1             localhost
127.0.1.1       %s.localdomain %s" "$hostname" "$hostname" >> /etc/hosts

clear
echo "Enter root password"
passwd
printf "Enter Username: "
read -r username
useradd -m -g wheel "$username"
echo "Enter user password"
passwd "$username"

clear
echo "Pre-Installation Finished, Reboot now"

